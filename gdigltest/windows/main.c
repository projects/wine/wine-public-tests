#include <windows.h>
#include <GL/gl.h>
#include <GL/glu.h>

LRESULT CALLBACK WndProc (HWND hWnd, UINT message,
						  WPARAM wParam, LPARAM lParam);
LRESULT CALLBACK ChildProc (HWND hWnd, UINT message,
							WPARAM wParam, LPARAM lParam);

void EnableOGL (HWND hWnd, HDC *hDC, HGLRC *hRC);
void DisableOGL (HWND hWnd, HDC hDC, HGLRC hRC);


#define               ID_CHILD 100

HDC               dc = 0;
HGLRC             hrc;
float             theta;
POINT             start_point, end_point;
BOOL              enable_rop = TRUE;
HPEN              solid_pen = 0;
HPEN              dot_pen   = 0;
HPEN              flush_pen = 0;
HINSTANCE         hInst;


void RenderModel()
{
	int i;

	for ( i = 0; i < 100; i++ )
	{
		glPushMatrix ();
		glRotatef(theta, 0.0f, 0.0f, 1.0f);
		glTranslatef(0.0f, 0.0f, (GLfloat)i/50);
		glBegin(GL_TRIANGLES);
		glColor3f(1.0f, 0.0f, 0.0f); glVertex2f(0.0f, 1.0f);
		glColor3f(0.0f, 1.0f, 0.0f); glVertex2f(0.87f, -0.5f);
		glColor3f(0.0f, 0.0f, 1.0f); glVertex2f(-0.87f, -0.5f);
		glEnd();
		glPopMatrix ();
	}
}


void DrawOGL()
{
	wglMakeCurrent( dc, hrc );

	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT);

	glMatrixMode( GL_PROJECTION );
	glLoadIdentity(); /* dummy */
	glMatrixMode( GL_MODELVIEW );

	RenderModel();

	SwapBuffers(dc);

	glFinish();
}


void SelectOGL()
{
	GLuint buf[512];
	wglMakeCurrent( dc, hrc );

	glSelectBuffer( 512, buf );
	glRenderMode( GL_SELECT );
	glInitNames();

	glMatrixMode( GL_PROJECTION );
	glPushMatrix();
	glLoadIdentity(); /* dummy */
	glMatrixMode( GL_MODELVIEW );

	RenderModel();

	glMatrixMode( GL_PROJECTION );
	glPopMatrix();
	glMatrixMode( GL_MODELVIEW );

	glRenderMode( GL_RENDER );

	/* glFinish(); */
}


void DrawGDI1()
{
	HPEN oldpen;

	if ( !solid_pen )
		solid_pen = CreatePen(PS_SOLID, 2, RGB(0, 255, 0));

	oldpen = (HPEN)SelectObject(dc, solid_pen);

	if ( !flush_pen )
		flush_pen = oldpen;

	SetROP2(dc, R2_COPYPEN);

	MoveToEx(dc, 20, 20, NULL);

	LineTo(dc, 600, 420 );
}


void DrawGDI2( BOOL hide )
{
	POINT tmp;
	HPEN oldpen;


	if ( !dot_pen )
		dot_pen = CreatePen(PS_DOT, 1, RGB(255, 0, 0));

	oldpen = (HPEN)SelectObject(dc, dot_pen);

	if ( !flush_pen )
		flush_pen = oldpen;

	SetROP2(dc, R2_XORPEN);

	if ( hide )
	{
		MoveToEx(dc, end_point.x-50, end_point.y, &tmp);
		LineTo(dc, end_point.x+50, end_point.y);
		MoveToEx(dc, end_point.x, end_point.y-50, &tmp);
		LineTo(dc, end_point.x, end_point.y+50);
	}

	MoveToEx(dc, start_point.x-50, start_point.y, &tmp);

	LineTo(dc, start_point.x+50, start_point.y);
	MoveToEx(dc, start_point.x, start_point.y-50, &tmp);
	LineTo(dc, start_point.x, start_point.y+50);

	end_point.x = start_point.x;
	end_point.y = start_point.y;

}


void FlushGDI()
{
	GdiFlush();

	if ( flush_pen )
	{
		SelectObject(dc, flush_pen);
		flush_pen = 0;
	}

	if ( solid_pen )
	{
		DeleteObject(solid_pen);
		solid_pen = 0;
	}

	if ( dot_pen )
	{
		DeleteObject(dot_pen);
		dot_pen = 0;
	}
}

int WINAPI WinMain (HINSTANCE hInstance,
					HINSTANCE hPrevInstance,
					LPSTR lpCmdLine,
					int iCmdShow)
{
	WNDCLASS wc;
	HWND hWnd;

	MSG msg;
	BOOL bQuit = FALSE;
	hInst = hInstance;
	wc.style = CS_OWNDC;
	wc.lpfnWndProc = WndProc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = hInstance;
	wc.hIcon = LoadIcon (NULL, IDI_APPLICATION);
	wc.hCursor = LoadCursor (NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH) GetStockObject (BLACK_BRUSH);
	wc.lpszMenuName = NULL;
	wc.lpszClassName = "MAIN_GLSample";
	RegisterClass (&wc);

	hWnd = CreateWindow (
			   "MAIN_GLSample", "OpenGL Sample - Main window",
			   WS_CAPTION | WS_CLIPSIBLINGS |WS_SYSMENU |
			   WS_VISIBLE | WS_OVERLAPPED | WS_THICKFRAME |
			   WS_MINIMIZEBOX | WS_MAXIMIZEBOX |0x00008000,
			   0, 0, 1024, 768,
			   NULL, NULL, hInstance, NULL);

	while (!bQuit)
	{
		if (PeekMessage (&msg, NULL, 0, 0, PM_REMOVE))
		{

			if (msg.message == WM_QUIT)
			{
				bQuit = TRUE;
			}
			else
			{
				TranslateMessage (&msg);
				DispatchMessage (&msg);
			}
		}
	}

	DestroyWindow(hWnd);

	return msg.wParam;
}

LRESULT CALLBACK WndProc (HWND hWnd, UINT message,
						  WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
		case WM_CREATE:
			{
				WNDCLASS w;
				HWND child;
				memset(&w,0,sizeof(WNDCLASS));
				w.lpfnWndProc = ChildProc;
				w.hInstance = hInst;
				w.hbrBackground = (HBRUSH)/* GetStockObject */(COLOR_WINDOW);
				w.lpszClassName = "ChildWClass";
				w.hCursor=LoadCursor (NULL, IDC_ARROW);
				RegisterClass(&w);

				child=CreateWindow("ChildWClass", "TEST", WS_CAPTION |
								   WS_CHILDWINDOW | WS_VISIBLE |
								   WS_CLIPSIBLINGS | WS_SYSMENU |
								   WS_THICKFRAME | WS_MINIMIZEBOX |
								   WS_MAXIMIZEBOX | 0x0000C000,5, 5, 400, 300, hWnd,(HMENU) ID_CHILD, hInst, NULL);
				ShowWindow(child,SW_NORMAL);
				UpdateWindow(child);

				start_point.x = end_point.x = 320;
				start_point.y = end_point.y = 240;

				EnableOGL(child, &dc, &hrc);

				return 0;
			}

		case WM_CLOSE:
			PostQuitMessage (0);
			return 0;

		case WM_DESTROY:
			return 0;

		case WM_KEYDOWN:

			switch (wParam)
			{
				case VK_ESCAPE:
					PostQuitMessage(0);
					return 0;

				case VK_LEFT:
					theta += 10.0f;
					break;
				case VK_RIGHT:
					theta -= 10.0f;
					break;
			}

			return 0;

			/*    case WM_ERASEBKGND:
			        return TRUE;*/

		default:
			return DefWindowProc (hWnd, message, wParam, lParam);
	}
}

LRESULT CALLBACK ChildProc (HWND hWnd, UINT message,
							WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
		case WM_DESTROY:
			DisableOGL(hWnd, dc, hrc);
			return 0;
		case WM_PAINT:
			theta += 10.0f;
			RedrawWindow( hWnd, NULL, NULL, RDW_INVALIDATE );
			PAINTSTRUCT ps;
			BeginPaint(hWnd, &ps);

			DrawOGL();

			DrawGDI1();
			DrawGDI2(FALSE);
			FlushGDI();

			EndPaint(hWnd, &ps);
			break;
		case WM_MOUSEMOVE:
			{
				start_point.x = LOWORD(lParam) - 20;
				start_point.y = HIWORD(lParam) - 20;

				SelectOGL();

				DrawGDI2(TRUE);
				FlushGDI();

				break;
			}

		case WM_KEYDOWN:

			switch (wParam)
			{
				case VK_ESCAPE:
					PostQuitMessage(0);
					return 0;

					/*        case VK_LEFT:
					            theta += 10.0f;
					//AR             DrawOGL();
					            InvalidateRect(hWnd, NULL, false);
					            break;
					        case VK_RIGHT:
					            theta -= 10.0f;
					//AR             DrawOGL();
					            InvalidateRect(hWnd, NULL, false);
					            break;     */
			}

		case WM_SIZE:
			glViewport(0,0,LOWORD(lParam),HIWORD(lParam));

			glMatrixMode(GL_PROJECTION);

			glLoadIdentity();

			gluPerspective(45.0f,(GLfloat)LOWORD(lParam)/(GLfloat)HIWORD(lParam),0.1f,100.0f);

			glMatrixMode(GL_MODELVIEW);

			glLoadIdentity();

			InvalidateRect(hWnd, NULL, FALSE);

			break;

			/*    case WM_ERASEBKGND:
			        return FALSE;    */

		default:
			return DefWindowProc (hWnd, message, wParam, lParam);
	}
}

void EnableOGL (HWND hWnd, HDC *hDC, HGLRC *hRC)
{
	PIXELFORMATDESCRIPTOR pfd;
	int iFormat;

	*hDC = GetDC (hWnd);

	SetMapMode( *hDC, MM_TEXT );
	SetBkColor( *hDC, RGB(0xff,0xff,0xff) );
	SetBkMode ( *hDC, TRANSPARENT );

	ZeroMemory (&pfd, sizeof (pfd));
	pfd.nSize = sizeof (pfd);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 24;
	pfd.cDepthBits = 32;
	pfd.cAlphaBits = 8;
	pfd.iLayerType = PFD_MAIN_PLANE;
	iFormat = ChoosePixelFormat (*hDC, &pfd);
	SetPixelFormat (*hDC, iFormat, &pfd);

	*hRC = wglCreateContext( *hDC );
}


void DisableOGL (HWND hWnd, HDC hDC, HGLRC hRC)
{
	wglDeleteContext (hRC);
	ReleaseDC (hWnd, hDC);
}

