#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>

#include <X11/X.h>
#include <X11/Xlib.h>


#include <stdio.h>
#include <stdlib.h>


static GLfloat spin = 0.0;

// win begin

#define ID_CHILD 100

float theta;

void RenderModel(){
    int i;

    printf("RenderModel begin\n");
    for ( i = 0; i < 100; i++ )
    {
        glPushMatrix ();
        glRotatef(theta, 0.0f, 0.0f, 1.0f);
        glTranslatef(0.0f, 0.0f, (GLfloat)i/50);
        glBegin(GL_TRIANGLES);
        glColor3f(1.0f, 0.0f, 0.0f); glVertex2f(0.0f, 1.0f);
        glColor3f(0.0f, 1.0f, 0.0f); glVertex2f(0.87f, -0.5f);
        glColor3f(0.0f, 0.0f, 1.0f); glVertex2f(-0.87f, -0.5f);
        glEnd();
        glPopMatrix ();
    }
    printf("RenderModel end\n");
}



void DrawOGL(){
    //wglMakeCurrent( dc, hrc );

    printf("DrawOGL begin\n");
    glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    glMatrixMode( GL_PROJECTION );
    glLoadIdentity(); /* dummy */
    glMatrixMode( GL_MODELVIEW );

    RenderModel();

    // SwapBuffers(dc);
    glutSwapBuffers();

    glFinish();
    printf("DrawOGL end\n");
}

void SelectOGL(void){
    GLuint buf[512];
    //mib wglMakeCurrent( dc, hrc );

    printf("SelectOGL begin\n");
    glSelectBuffer( 512, buf );
    glRenderMode( GL_SELECT );
    glInitNames();

    glMatrixMode( GL_PROJECTION );
    glPushMatrix();
    glLoadIdentity(); /* dummy */
    glMatrixMode( GL_MODELVIEW );

    RenderModel();

    glMatrixMode( GL_PROJECTION );
    glPopMatrix();
    glMatrixMode( GL_MODELVIEW );

    glRenderMode( GL_RENDER );
    /* glFinish(); */
    printf("SelectOGL end\n");
}




// win end




/* void init(void){
	glClearColor (0.0, 0.0, 0.0, 0.0);
	glShadeModel (GL_FLAT);
} */



/*void display(void){
	glClear(GL_COLOR_BUFFER_BIT);
	glPushMatrix();
	glRotatef(spin, 0.0, 0.0, 1.0);
	glColor3f(1.0, 1.0, 1.0);
	glRectf(-25.0, -25.0, 25.0, 25.0);
	glPopMatrix();
	glutSwapBuffers();
}
*/

void spinDisplay(void){
	spin = spin + 2.0;

	if (spin > 360.0)
		spin = spin - 360.0;

	glutPostRedisplay();
}

/*void reshape(int w, int h){
	glViewport (0, 0, (GLsizei) w, (GLsizei) h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(-50.0, 50.0, -50.0, 50.0, -1.0, 1.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}
*/


void reshape(int x, int y){
    printf("reshape begin\n");

    { // rmo ->
        theta += 10.0f;
        DrawOGL();
    }

    //glViewport(0,0,LOWORD(lParam),HIWORD(lParam));
    glViewport(0, 0, (GLsizei)x, (GLsizei)y);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    // gluPerspective(45.0f, (GLfloat)LOWORD(lParam)/(GLfloat)HIWORD(lParam), 0.1f,100.0f);
    gluPerspective(45.0f, ((GLfloat)x)/((GLfloat)y), 0.1f, 100.0f);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    printf("reshape end\n");
}

void mouse(int button, int state, int x, int y){
	switch (button)	{
		case GLUT_LEFT_BUTTON:

			if (state == GLUT_DOWN)
				glutIdleFunc(spinDisplay);

			break;

		case GLUT_RIGHT_BUTTON:
			glViewport(50,50,200,100);

			break;

		case GLUT_MIDDLE_BUTTON:
			if (state == GLUT_DOWN)
				glutIdleFunc(NULL);

			break;

		default:
			break;
	}
}


void key(unsigned char key, int x, int y){
    switch(key){
        case '[':theta += 10.0f;
            break;
        case ']':theta -= 10.0f;
            break;
        case 'q':exit(0);
    }
}


/*
* double buffer display mode.
* Register mouse input callback functions
*/
int main(int argc, char** argv){
{
    Display *display;
    Window window, rootwindow;
    int screen;

    display = XOpenDisplay(NULL);
    screen = DefaultScreen(display);
    rootwindow = RootWindow(display, screen);
    window = XCreateSimpleWindow(
        display,
        rootwindow,
        0,
        0,
        100,
        100,
        1,
        0,
        0
    );
    XMapWindow(display, window);
    XFlush(display);

    sleep(10);
    XCloseDisplay(display);

}




	glutInit(&argc, argv);
	glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGB);
	glutInitWindowSize (250, 250);
	glutInitWindowPosition (100, 100);
	glutCreateWindow (argv[0]);
	
    SelectOGL();

// callbacks	
    glutDisplayFunc(RenderModel);

	glutReshapeFunc(reshape);
	glutMouseFunc(mouse);
    glutKeyboardFunc(key);

	glutMainLoop();
	return 0;
}