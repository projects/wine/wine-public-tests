#include <windows.h>
#include <GL/gl.h>
#include <GL/glu.h>

LRESULT CALLBACK WndProc (
    HWND hWnd,
    UINT message,
    WPARAM wParam,
    LPARAM lParam
){ return DefWindowProc(hWnd, message, wParam, lParam); }

int WINAPI WinMain(
    HINSTANCE hInstance,
    HINSTANCE hPrevInstance,
    LPSTR lpCmdLine,
    int iCmdShow
){
    /* window */
    WNDCLASS wc;
    HWND hWnd;

    /* gl */
    HDC              hDC = 0;
    HGLRC            hRC;

    /* window class init */
    wc.style         = CS_OWNDC;
    wc.lpfnWndProc   = WndProc;
    wc.cbClsExtra    = 0;
    wc.cbWndExtra    = 0;
    wc.hInstance     = hInstance;
    wc.hIcon         = LoadIcon (NULL, IDI_APPLICATION);
    wc.hCursor       = LoadCursor (NULL, IDC_ARROW);
    wc.hbrBackground = (HBRUSH) GetStockObject (BLACK_BRUSH);
    wc.lpszMenuName  = NULL;
    wc.lpszClassName = "MAIN_GLSample";

    RegisterClass (&wc);


    /* create window */
    hWnd = CreateWindow (
        "MAIN_GLSample",
        "OpenGL Sample - Main window",
        WS_CAPTION | WS_CLIPSIBLINGS |WS_SYSMENU |
        WS_VISIBLE | WS_OVERLAPPED | WS_THICKFRAME |
        WS_MINIMIZEBOX | WS_MAXIMIZEBOX |0x00008000,
        0, 0, 800, 600,
        NULL, NULL, hInstance, NULL
    );

    /* enable gl */
    {
        PIXELFORMATDESCRIPTOR pfd;
        int iFormat;

        hDC = GetDC (hWnd);

        SetMapMode( hDC, MM_TEXT );
        SetBkColor( hDC, RGB(0xff,0xff,0xff) );
        SetBkMode ( hDC, TRANSPARENT );

        /* fill pfd */
        ZeroMemory (&pfd, sizeof (pfd));
        pfd.nSize = sizeof (pfd);
        pfd.nVersion = 1;
        pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
        pfd.iPixelType = PFD_TYPE_RGBA;
        pfd.cColorBits = 24;
        pfd.cDepthBits = 32;
        pfd.cAlphaBits = 8;
        pfd.iLayerType = PFD_MAIN_PLANE;


        MessageBoxA(NULL, "Press [OK] and wait 10 sec. (call ChoosePixelFormat(hDC, &pfd) )", "Debug", MB_OK);
        iFormat = ChoosePixelFormat (hDC, &pfd);
        SetPixelFormat (hDC, iFormat, &pfd);

        /* create context */
        hRC = wglCreateContext( hDC );
    }

    Sleep(10000);

    /* disable gl */
    {
        wglDeleteContext (hRC);
        ReleaseDC (hWnd, hDC);
    }

    DestroyWindow(hWnd);
}