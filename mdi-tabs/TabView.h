#if !defined(AFX_TABVIEW_H__3E82FAEB_36B2_4126_972D_09A866A7C588__INCLUDED_)
#define AFX_TABVIEW_H__3E82FAEB_36B2_4126_972D_09A866A7C588__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TabView.h : header file
//
#define TAB_ID 1000
//#include ""
/////////////////////////////////////////////////////////////////////////////
// CTabView view

class CTabView : public CView
{
protected:
	CTabView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CTabView)

// Attributes
public:

	//BOOL Create(LPCTSTR lpszClassName,LPCSTR lpzWindowName, DWORD dwStyle, const Rect& rect,CWnd *pParentWnd,UINT nID, CCreateContext *pContext);
    CTabCtrl cTab;
	void CreateItems();
// Operations
public:
	
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTabView)
	public:
	virtual BOOL Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext );
	protected:
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
	virtual void CalcWindowRect(LPRECT lpClientRect, UINT nAdjustType );
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CTabView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
protected:
	//{{AFX_MSG(CTabView)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TABVIEW_H__3E82FAEB_36B2_4126_972D_09A866A7C588__INCLUDED_)
