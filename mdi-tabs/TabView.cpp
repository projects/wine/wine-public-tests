// TabView.cpp : implementation file
//

#include "stdafx.h"
#include "TestView.h"
#include "TabView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTabView

IMPLEMENT_DYNCREATE(CTabView, CView)

CTabView::CTabView()
{
}

CTabView::~CTabView()
{
}


BEGIN_MESSAGE_MAP(CTabView, CView)
	//{{AFX_MSG_MAP(CTabView)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTabView drawing

void CTabView::OnDraw(CDC* pDC)
{
	CDocument* pDoc = GetDocument();
	// TODO: add draw code here
}

/////////////////////////////////////////////////////////////////////////////
// CTabView diagnostics

#ifdef _DEBUG
void CTabView::AssertValid() const
{
	CView::AssertValid();
}

void CTabView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CTabView message handlers

//переопределяем конструктор
/*BOOL CTabView::Create(LPCTSTR lpszClassName, 
		LPCTSTR lpszWindowName, DWORD dwStyle,
		const RECT& rect, CWnd* pParentWnd,
		UINT nID, CCreateContext* pContext) 
{
// TODO: Add your specialized code here and/or call the base class


	return TRUE;
}*/

void CTabView::CreateItems()
{
	TC_ITEM tc;
	tc.mask=TCIF_TEXT;
	CString str1="Nomer 1";
	tc.pszText=(LPSTR)(LPCTSTR)str1;
	tc.cchTextMax=str1.GetLength();
	cTab.InsertItem(0,&tc);
	tc.mask=TCIF_TEXT;
	str1="Nomer 2";
	tc.pszText=(LPSTR)(LPCTSTR)str1;
	tc.cchTextMax=str1.GetLength();
	cTab.InsertItem(1,&tc); 
}

/*
void CTabView::CalcWindowRect(LPRECT lpClientRect, UINT nAdjustType) 
{
	
}
*/

BOOL CTabView::Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext) 
{
	// TODO: Add your specialized code here and/or call the base class
	
		CWnd::Create(lpszClassName, lpszWindowName, dwStyle,
		rect, pParentWnd, nID, pContext);
		cTab.Create(dwStyle,rect,pParentWnd,nID);
//	if (cTab.Create(WS_CHILD | WS_VISIBLE|TCS_BOTTOM,
//		rect,this,TAB_ID)!=TRUE) 
//	{
//		AfxMessageBox("Error Create cTab"); 
//		return FALSE;
//	}
   
	CreateItems();
  // return CWnd::Create(lpszClassName, lpszWindowName, dwStyle, rect, pParentWnd, nID, pContext);
  return TRUE;
}

void CTabView::CalcWindowRect(LPRECT lpClientRect, UINT nAdjustType) 
{
	CView::CalcWindowRect(lpClientRect, nAdjustType);

//	CRect rect=lpClientRect;
	//cTab.MoveWindow(0,0,rect.Width()-4,rect.Height()-4);
// TODO: Add your specialized code here and/or call the base class
	//CView::CalcWindowRect(
    CView::CalcWindowRect(lpClientRect, nAdjustType);
}
